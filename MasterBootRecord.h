#pragma once

const size_t MBR_BOOTSTRAP_SIZE = 440;
const size_t MBR_DISK_SIGNATURE_SIZE = 4;
const size_t MBR_COPY_PROTECTED_SIZE = 2;
const size_t MBR_PARTITIONS_SIZE = 64;
const size_t MBR_BOOT_SIGNATURE_SIZE = 2;

const size_t MBR_SIZE = MBR_BOOTSTRAP_SIZE + MBR_DISK_SIGNATURE_SIZE + MBR_COPY_PROTECTED_SIZE + MBR_PARTITIONS_SIZE + MBR_BOOT_SIGNATURE_SIZE;

const size_t MBR_PARTITION_ENTRY_COUNT = 4;

const size_t MBR_PARTITION_ENTRY_STATUS_SIZE = 1;
const size_t MBR_PARTITION_ENTRY_CHS_FIRST_SIZE = 3;
const size_t MBR_PARTITION_ENTRY_TYPE_SIZE = 1;
const size_t MBR_PARTITION_ENTRY_CHS_LAST_SIZE = 3;
const size_t MBR_PARTITION_ENTRY_LBA_SIZE = 4;
const size_t MBR_PARTITION_ENTRY_SECTORS_SIZE = 4;

const size_t MBR_PARTITION_ENTRY_SIZE = MBR_PARTITION_ENTRY_STATUS_SIZE + MBR_PARTITION_ENTRY_CHS_FIRST_SIZE + MBR_PARTITION_ENTRY_TYPE_SIZE + MBR_PARTITION_ENTRY_CHS_LAST_SIZE + MBR_PARTITION_ENTRY_LBA_SIZE + MBR_PARTITION_ENTRY_SECTORS_SIZE;

#pragma pack (push, 1)
typedef struct PartitionEntry {

	unsigned char status;
	unsigned char chs_first[3];
	unsigned char type;
	unsigned char chs_last[3];
	unsigned char lba[4];
	unsigned char sectors[4];

} PartitionEntry;

typedef struct MasterBootRecord {

	unsigned char bootstrap[440];
	unsigned char disk_signature[4];
	unsigned char copy_protected[2];
	PartitionEntry partitions[4];
	unsigned char boot_signature[2];

} MasterBootRecord;
#pragma pack (pop)