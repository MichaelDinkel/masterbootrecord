#pragma once

#include <iostream>
#include <iomanip>
#include <memory>
#include <string>
#include <vector>
#include "Windows.h"
#include "MasterBootRecord.h"

class DriveReader {

private:

	MasterBootRecord mbr;

	void printPartitionEntry(size_t index, PartitionEntry entry);

public:

	DriveReader(std::wstring drive);
	void printMbr();

};