#include "DriveReader.h"

DriveReader::DriveReader(std::wstring name) {

	SecureZeroMemory(&mbr, sizeof(MasterBootRecord));

	wchar_t * win32DriveName = new wchar_t[name.length() + 5];
	wcscpy_s(win32DriveName, name.length() + 5, L"\\\\.\\");
	wcscat_s(win32DriveName, name.length() + 5, name.c_str());

	HANDLE drive = CreateFile(win32DriveName, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	delete[] win32DriveName;

	if (drive == INVALID_HANDLE_VALUE) {

		std::cout << GetLastError() << std::endl;

	} else {

		DISK_GEOMETRY geometry {};
		SecureZeroMemory(&geometry, sizeof(DISK_GEOMETRY));
		DWORD bytesReturned = 0UL;

		if (DeviceIoControl(drive, IOCTL_DISK_GET_DRIVE_GEOMETRY, NULL, 0, &geometry, sizeof(DISK_GEOMETRY), &bytesReturned, NULL)) {

			DWORD bytesPerSector = geometry.BytesPerSector;
			unsigned int sectorsNeeded = MBR_SIZE / bytesPerSector + (MBR_SIZE % bytesPerSector != 0);

			std::vector<std::shared_ptr<unsigned char[]>> sectors;

			for (unsigned int i = 0; i < sectorsNeeded; i++) {

				std::shared_ptr<unsigned char[]> sector(new unsigned char[bytesPerSector]);

				DWORD read = 0;
				BOOL readResult = ReadFile(drive, sector.get(), bytesPerSector, &read, NULL);

				if (!readResult || read != bytesPerSector) {

					std::cout << GetLastError() << std::endl;

				} else {

					sectors.push_back(sector);

				}

			}

			std::unique_ptr<unsigned char[]> mbrBytes(new unsigned char[MBR_SIZE]);

			size_t current = 0;
			unsigned int currentSector = 0;

			while (current < MBR_SIZE) {

				std::memcpy(mbrBytes.get(), sectors[currentSector].get(), MBR_SIZE < bytesPerSector ? MBR_SIZE : bytesPerSector);
				current += bytesPerSector;
				currentSector++;

			}

			std::memcpy(&mbr, mbrBytes.get(), MBR_SIZE);

		} else {

			std::cout << GetLastError() << std::endl;

		}

	}

}

void printPartitionEntry(size_t index, PartitionEntry entry) {

	std::cout << "\tPartition " << index << ": " << std::endl;
	
	std::cout << "\t\tStatus: " << std::uppercase << std::setw(2) << std::setfill('0') << std::hex;
	std::cout << (int) entry.status << std::endl;


	std::cout << "\t\tCHS begin address: " << std::uppercase << std::setw(2) << std::setfill('0') << std::hex;

	for (size_t i = 0; i < MBR_PARTITION_ENTRY_CHS_FIRST_SIZE; i++) {

		std::cout << std::setw(2) << std::setfill('0') << (int) entry.chs_first[i] << " ";

	}

	std::cout << std::endl;


	std::cout << "\t\tType: " << std::uppercase << std::setw(2) << std::setfill('0') << std::hex;
	std::cout << (int) entry.type << std::endl;


	std::cout << "\t\tCHS last address: " << std::uppercase << std::setw(2) << std::setfill('0') << std::hex;

	for (size_t i = 0; i < MBR_PARTITION_ENTRY_CHS_LAST_SIZE; i++) {

		std::cout << std::setw(2) << std::setfill('0') << (int) entry.chs_last[i] << " ";

	}

	std::cout << std::endl;


	std::cout << "\t\tPartition start LBA: " << std::uppercase << std::setw(2) << std::setfill('0') << std::hex;

	for (size_t i = 0; i < MBR_PARTITION_ENTRY_LBA_SIZE; i++) {

		std::cout << std::setw(2) << std::setfill('0') << (int) entry.lba[i] << " ";

	}

	std::cout << std::endl;


	std::cout << "\t\tSectors: " << std::uppercase << std::setw(2) << std::setfill('0') << std::hex;

	for (size_t i = 0; i < MBR_PARTITION_ENTRY_SECTORS_SIZE; i++) {

		std::cout << std::setw(2) << std::setfill('0') << (int) entry.sectors[i] << " ";

	}

	uint32_t decimalSectors = 0;
	std::memcpy(&decimalSectors, entry.sectors, MBR_PARTITION_ENTRY_SECTORS_SIZE);

	std::cout << std::setw(1) << std::dec << "(" << decimalSectors << ")" << std::endl;


	std::cout << std::endl;

}

void DriveReader::printMbr() {

	std::cout << std::endl << "Bootstrap bytes: " << std::uppercase << std::setw(2) << std::setfill('0') << std::hex;

	for (size_t i = 0; i < MBR_BOOTSTRAP_SIZE; i++) {

		std::cout << (int) mbr.bootstrap[i] << " ";

	}

	std::cout << std::endl << std::endl;


	std::cout << "Disk signature: " << std::uppercase << std::setw(2) << std::setfill('0') << std::hex;

	for (size_t i = 0; i < MBR_DISK_SIGNATURE_SIZE; i++) {

		std::cout << (int) mbr.disk_signature[i] << " ";

	}

	std::cout << std::endl << std::endl;


	std::cout << "Copy protected: " << std::uppercase << std::setw(2) << std::setfill('0') << std::hex;

	for (size_t i = 0; i < MBR_COPY_PROTECTED_SIZE; i++) {

		std::cout << (int) mbr.copy_protected[i] << " ";

	}

	std::cout << std::endl << std::endl;


	std::cout << "Partitions: " << std::endl;

	for (size_t i = 0; i < MBR_PARTITION_ENTRY_COUNT; i++) {

		printPartitionEntry(i, mbr.partitions[i]);

	}


	std::cout << "Boot signature: " << std::uppercase << std::setw(2) << std::setfill('0') << std::hex;

	for (size_t i = 0; i < MBR_BOOT_SIGNATURE_SIZE; i++) {

		std::cout << (int) mbr.boot_signature[i] << " ";

	}

	std::cout << std::endl << std::endl;

}

void DriveReader::printPartitionEntry(size_t index, PartitionEntry entry) {

	std::cout << "\tPartition " << index << ": " << std::endl;

	std::cout << "\t\tStatus: " << std::uppercase << std::setw(2) << std::setfill('0') << std::hex;
	std::cout << (int) entry.status << std::endl;


	std::cout << "\t\tCHS begin address: " << std::uppercase << std::setw(2) << std::setfill('0') << std::hex;

	for (size_t i = 0; i < MBR_PARTITION_ENTRY_CHS_FIRST_SIZE; i++) {

		std::cout << std::setw(2) << std::setfill('0') << (int) entry.chs_first[i] << " ";

	}

	std::cout << std::endl;


	std::cout << "\t\tType: " << std::uppercase << std::setw(2) << std::setfill('0') << std::hex;
	std::cout << (int) entry.type << std::endl;


	std::cout << "\t\tCHS last address: " << std::uppercase << std::setw(2) << std::setfill('0') << std::hex;

	for (size_t i = 0; i < MBR_PARTITION_ENTRY_CHS_LAST_SIZE; i++) {

		std::cout << std::setw(2) << std::setfill('0') << (int) entry.chs_last[i] << " ";

	}

	std::cout << std::endl;


	std::cout << "\t\tPartition start LBA: " << std::uppercase << std::setw(2) << std::setfill('0') << std::hex;

	for (size_t i = 0; i < MBR_PARTITION_ENTRY_LBA_SIZE; i++) {

		std::cout << std::setw(2) << std::setfill('0') << (int) entry.lba[i] << " ";

	}

	std::cout << std::endl;


	std::cout << "\t\tSectors: " << std::uppercase << std::setw(2) << std::setfill('0') << std::hex;

	for (size_t i = 0; i < MBR_PARTITION_ENTRY_SECTORS_SIZE; i++) {

		std::cout << std::setw(2) << std::setfill('0') << (int) entry.sectors[i] << " ";

	}

	uint32_t decimalSectors = 0;
	std::memcpy(&decimalSectors, entry.sectors, MBR_PARTITION_ENTRY_SECTORS_SIZE);

	std::cout << std::setw(1) << std::dec << "(" << decimalSectors << ")" << std::endl;


	std::cout << std::endl;

}