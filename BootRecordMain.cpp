#include "BootRecordMain.h"

void BootRecordMain::startUI(std::vector<std::wstring> drives) {

	std::cout << "Found " << drives.size() << " physical drives:" << std::endl << std::endl;

	for (size_t i = 0; i < drives.size(); i++) {

		std::wcout << L"\t" << i << L". " << drives[i] << std::endl;

	}

	std::cout << std::endl;

	int input = -2;

	while (input != -1) {

		std::wstring line;
		std::cout << "Enter the drive number to use or -1 to exit: ";
		std::getline(std::wcin, line);
		std::wstringstream converter(line);
		
		if (!(converter >> input) || ((input < 0 && input != -1) || input > (int) drives.size())) {
			
			std::cout << "Invalid drive ID." << std::endl;
			input = -2;

		} else {

			if (input != -1) {

				DriveReader reader(drives[input]);
				reader.printMbr();

			}

		}

	}

}

BootRecordMain::BootRecordMain() {

	DriveLoader driveLoader;
	startUI(driveLoader.getDrives());

}

int main() {

	BootRecordMain main;

}