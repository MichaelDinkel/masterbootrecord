#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include <Windows.h>

class DriveLoader {

private:

	std::vector<std::wstring> drives;

	bool isPhysicalDrive(wchar_t * str, size_t len);

public:

	DriveLoader();
	std::vector<std::wstring> getDrives();

};