#pragma once

#include <iostream>
#include <string>
#include <sstream>
#include "DriveLoader.h"
#include "DriveReader.h"

class BootRecordMain {

private:

	void startUI(std::vector<std::wstring> drives);

public:

	BootRecordMain();

};