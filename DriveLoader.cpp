#include "DriveLoader.h"

DriveLoader::DriveLoader() {

	std::unique_ptr<wchar_t[]> buffer(new wchar_t[1048576]);

	DWORD chars = QueryDosDevice(NULL, buffer.get(), 1048576);

	if (chars == 0) {

		std::cout << GetLastError() << std::endl;

	} else {

		size_t count = 0;

		size_t i = 0;

		while (i < 1048576) {

			size_t currentLength = wcslen(buffer.get() + i) + 1;
			std::unique_ptr<wchar_t[]> currentString(new wchar_t[currentLength]);

			wcscpy_s(currentString.get(), currentLength, buffer.get() + i);

			if (isPhysicalDrive(currentString.get(), currentLength)) {

				drives.push_back(std::wstring(currentString.get()));
				count++;

			}

			i += currentLength;

		}

	}

}

std::vector<std::wstring> DriveLoader::getDrives() {

	return drives;

}

bool DriveLoader::isPhysicalDrive(wchar_t * str, size_t len) {

	const wchar_t * physicalDrive = L"PhysicalDrive";
	const size_t physicalDriveLength = wcslen(physicalDrive);

	bool isDrive = false;

	if (len >= physicalDriveLength + 1) {

		bool bad = false;
		size_t i = 0;

		while (!bad && i < physicalDriveLength) {

			if (str[i] != physicalDrive[i]) {

				bad = true;

			}

			i++;

		}

		isDrive = !bad;

	}

	return isDrive;

}